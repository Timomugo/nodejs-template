import { Router } from "express";
import example from '@node-template/modules/example/api';

class ApiV1 {
    public router: Router;
    
    constructor() {
        this.router = Router();
        this.init();
    }
    
    public init(): Router {
        example(this.router);
        return this.router;
    }
}

export default ApiV1;