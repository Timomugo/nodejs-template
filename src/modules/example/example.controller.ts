import Controller from "@node-template/core/express/controller";
import { Request } from "express";
import ExampleService from "./example.service";

class ExampleController extends Controller {

    public example = (req: Request) => {
      return new ExampleService().get();
    };
}

export default ExampleController;