import { Router, Request, Response } from "express";
import ExampleController from "../example.controller";

const router = Router();

export default (app: Router) => {
    app.use('/', router);

    router.get('/', (req:Request, res: Response) => new ExampleController(req, res).example(req));
}