import { Router } from "express";
import examplesRoutes from "./examples.routes";

const exampleRouter = Router();

/**
 * TODO: This routing can be substituted for the default express routing
 * TODO: This can also be done by using dependency injection in the controllers
 */
export default (app: Router) =>{
    app.use('/example', exampleRouter);
    examplesRoutes(exampleRouter);
};