import ExampleRepository from "./example.repository";

class ExampleService {
    //TODO: outline all your use cases here

    private readonly repository: ExampleRepository; // This can be injected using dependecy injection

    constructor() {
        this.repository = new ExampleRepository();
    }

    public get = () => {
        return this.repository.getExample();
    }
}

export default ExampleService;